<?php

namespace Drupal\field_role_restriction\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\user\Entity\Role;

/**
 * FieldRoleRestriction Form.
 */
class FieldRoleRestrictionForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'field_role_restriction.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_role_restriction_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $node_types = NodeType::loadMultiple();
    // Loading administrator role.
    $roleObj = Role::load('administrator');
    // List out all roles.
    $roles = Role::loadMultiple();
    $role_list = [];
    foreach ($roles as $role => $rolesObj) {
      $role_list[] = $role;
    }
    // List of all fileds in system.
    $values = [];
    foreach ($node_types as $node_type) {
      $values[$node_type->id()] = $node_type->label();
      $entity_type_id = 'node';
      $bundle = $node_type->id();
      $definitions = (\Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle));
      foreach ($definitions as $key => $val) {
        $fieldNames[] = $key;
      }
    }
    $fieldNames = array_unique($fieldNames);
    // Removing the default fields from listing.
    foreach ($fieldNames as $fields) {
      if (strpos($fields, 'field_') !== FALSE) {
        $bundleFields[] = $fields;
      }
    }
    $bundleFields = array_unique($bundleFields);
    $config = $this->config('field_role_restriction.adminsettings');
    // The permissions table.
    // For the initial loading.
    if (!$config->get('permissions')) {
      $form['permissions'] = [
        '#type' => 'table',
        '#header' => [$this->t('Fields')],
        '#id' => 'permissions',
        '#attributes' => ['class' => ['permissions', 'js-permissions']],
        '#sticky' => TRUE,
      ];
      // Table columns.
      foreach ($role_list as $role) {
        $form['permissions']['#header'][] = [
          'data' => $role,
        ];
      }
      // Table row values.
      foreach ($bundleFields as $key1 => $field) {
        foreach ($role_list as $role) {
          $form['permissions'][$key1]['Field'] = [
            '#type' => 'textfield',
            '#default_value' => $field,
            '#attributes' => ['readonly' => 'readonly'],
          ];
          $form['permissions'][$key1][$role] = [
            '#type' => 'checkbox',
            'data' => $field,
          ];
        }
      }
    }
    $existingarray = [];
    $value = $config->get('permissions');
    foreach ($value as $field) {
      $existingarray[] = $field['Field'];
    }
    // Newly added fields after configuration save.
    $result = array_diff($bundleFields, $existingarray);
    // Removed fields after configuration save.
    $result1 = array_diff($existingarray, $bundleFields);
    // Removing the removed fields from listing.
    foreach ($value as $subKey => $subArray) {
      foreach ($result1 as $exvalue) {
        if ($subArray['Field'] == $exvalue) {
          unset($value[$subKey]);
        }
      }
    }
    // Including the newly added fields for listing.
    foreach ($result as $field) {
      $value[] = ["Field" => $field];
    }
    // To bind saved values from config.
    if ($config->get('permissions')) {
      $form['permissions'] = [
        '#type' => 'table',
        '#header' => [$this->t('Fields')],
        '#id' => 'permissions',
        '#attributes' => ['class' => ['permissions', 'js-permissions']],
        '#sticky' => TRUE,
      ];
      // Table columns.
      foreach ($role_list as $role) {
        $form['permissions']['#header'][] = [
          'data' => $role,
        ];
      }
      // Table row values.
      foreach ($value as $key1 => $field) {
        foreach ($role_list as $role) {
          $form['permissions'][$key1]['Field'] = [
            '#type' => 'textfield',
            '#default_value' => $field['Field'],
            '#attributes' => ['readonly' => 'readonly'],
            'class' => ['checkbox'],
          ];
          $form['permissions'][$key1][$role] = [
            '#type' => 'checkbox',
            'class' => ['checkbox'],
            '#default_value' => $field[$role],
          ];
        }
      }
    }
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $this->config('field_role_restriction.adminsettings')
      ->set('permissions', $form_state->getValue('permissions'))
      ->save();
    parent::submitForm($form, $form_state);

  }

}
